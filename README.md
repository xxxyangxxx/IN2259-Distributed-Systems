## [IN2259] Distributed Systems

This repository contains all the resources given to the students of Technische Universität München during the academic year 2018-2019.
Here you can find the slides and exercises downloaded from TUM's moodle and the solutions of said exercises.
All the exercises and its solutions have an extra version featuring Microsoft Office Word's proprietary format, and depending on the type of exercise MscGen or GraphViz DOT files.

The following file index has the original format and so the names do not match, because files have been renamed to have a more human-readable format.
Anyway, the links will guide you to the correct renamed files.

## Distributed Systems (IN2259)

**Lecture**

The lecture takes place every **Wednesday 4 pm – 7 pm (5620.01.101, Interims Hörsaal 1)**.

**Tutorial**

The tutorials for Distributed Systems are managed as a separate course in TUMonline (LV-No.: 0000001554).

Altogether we offer 10 tutorial sessions taking place on Thursdays and Fridays.
Please correctly enroll for the session you like to attend.
(Cf. TUMonline page for details).

Assignment 1 will be published in the week before the first tutorial takes place (24th of October).
The tutorials start in week three (2nd of November).

**Literature**

Throughout the course we are referring to the following books:

- [TS01] – Andrew S. Tanenbaum and Maarten van Steen. Distributed Systems: Principles and Paradigms. Prentice Hall International (September 2001).
- [CD11+] – George Coulouris, Jean Dollimore, Tim Kindberg and Gordon Blair. Distributed Systems: Concepts and Design. 5th edition. Prentice Hall (27. April 2011)

Relevant chapters and additional literature will be announced separately.

------

### 17th October 2018 – 23rd October 2018

- [Lecture 1 – Introduction][slide-1]

**Required:**

- **Either Chapter 1 TS or Chapter 1 CDKB**
- **[<u>Fallacies of Distributed Computing Explained By Arnon Rotem-Gal-Oz</u>][required-1-1]**
- **Sections 1-5 (other sections recommended) in [<u>Bigtable: A Distributed Storage System for Structured Data</u>][required-1-2], OSDI'06**
- **Section 1 in Lars George: [<u>HBase: The Definitive Guide</u>][required-1-3] ([<u>Link</u>][required-1-4]) O'Reilly Media, Inc., 2011**

**Recommended:**

- [<u>Deutsch's Fallacies, 10 Years After</u>][recommended-1-1]
- [<u>NoSQL Overview (in German)</u>][recommended-1-2]
- [<u>Apache Wiki on Cassandra</u>][recommended-1-3]
- [<u>Cassandra by example (slides)</u>][recommended-1-4]
- [<u>Google Spanner</u>][recommended-1-5]

------

### 24th October 2018 – 30th October 2018

- [Lecture 2 – Time][slide-2]
- [Assignment 1 – Physical Clocks][assignment-1]
- [Assignment 1 – Physical Clocks – Solution][solution-1]

**Required:**

- **Either 14.1-14.4 in CDKB or 5.1-5.2 in TS.**

**Recommended:**

- **Leslie Lamport, "Time, clocks, and the ordering of events in a distributed system”, Communications of the ACM, Vol. 27, No. 7, July 1978, pp. 558-565.**
- **Colin J. Fidge, "Timestamps in Message-Passing Systems That Preserve the Partial Ordering". In the 11th Australian Computer Science Conference, February, 1988. pp. 56-66.**
- **Friedemann Mattern, "Virtual Time and Global States of Distributed Systems". In Workshop on Parallel and Distributed Algorithms, October, 1988, Chateau de Bonas, France: Elsevier, pp. 215-226.**

------

### 31st October 2018 – 6th November 2018

- [Lecture 3 – Coordination][slide-3]
- [Assignment 2 – Logical Clocks][assignment-2]
- [Assignment 2 – Logical Clocks – Solution][solution-2]

**Required:**

- **Either 15 in CDKB or 5.4, 5.5 in TS (consensus is missing in TS).**

------

### 7th November 2018 – 13th November 2018

- [Lecture 4 – Consensus with Paxos][slide-4]
- [Assignment 3 – Coordination][assignment-3]
- [Assignment 3 – Coordination – Solution][solution-3]

**Required:**

- **Lamport, Leslie (2001). [<u>Paxos Made Simple</u>][required-4] ACM SIGACT News (Distributed Computing Column) 32, 4 (Whole Number 121, December 2001) 51-58.**

**Recommended:**

- **[<u>https://en.wikipedia.org/wiki/Paxos\_(computer\_science)</u>][recommended-4]**

------

### 14th November 2018 – 20th November 2018

- [Lecture 5 – Replication][slide-5]
- [Assignment 4 – Paxos][assignment-4]
- [Assignment 4 – Paxos – Solution][solution-4]

**Recommended:**

- **(Gossiping in Cassandra, start at 11:04-21:02)**

  [![Apple Inc.: Cassandra Internals — Understanding Gossip][recommended-5-2]][recommended-5-1]

------

### 21st November 2018 – 27th November 2018

- [Lecture 6 – Consistency][slide-6]
- [Assignment 5 – Replication][assignment-5]
- [Assignment 5 – Replication – Solution][solution-5]

**Required:**

- Either Ch. 18 (Distributed shared Memory) in CDKB or Ch 7 (Consistency & Replication) in TS
- Either Ch 16.2 in CDKB (Transactions) or Ch 8.5 in TS (Commit Protocols)

**Recommended:**

- **(CRDT, starts at 11:55-18:54)**

  [![Consistency without consensus in production systems][recommended-6-2]][recommended-6-1]

- Jim Gray and Leslie Lamport. Consensus on Transaction Commit: [<u>https://www.microsoft.com/en-us/research/wp-content/uploads/2016/02/tr-2003-96.pdf</u>][recommended-6-3]
- Session Guarantees for Weakly Consistent Replicated Data. In Proceeding PDIS '94 Proceedings of the third international conference on on Parallel and distributed information systems. pp. 140-150 Douglas B. Terry, Alan J. Demers, Karin Petersen, Mike J. Spreitzer, Marvin M. Theimer, and Brent B. Welch: [<u>http://www.cs.cornell.edu/courses/cs734/2000fa/cached%20papers/SessionGuaranteesPDIS_1.html</u>][recommended-6-4]

------

### 28th November 2018 – 4th December 2018

- [Lecture 7 – Web Caching and Consistent Hashing][slide-7]
- [Assignment 6 – Consistency][assignment-6]
- [Assignment 6 – Consistency – Solution][solution-6]

**Required:**

- [<u>https://henryr.github.io/cap-faq/</u>][required-7]

**Recommended:**

- D. Karger, et al. Consistent hashing and random trees: Distributed caching protocols for relieving hot spots on the World Wide Web. In Proceedings of the Twenty-Ninth Annual ACM Symposium on Theory of Computing, pages 654-663 , 1997.

------

### 5th December 2018 – 11th December 2018

- [Lecture 8 – MapReduce][slide-8]
- [Assignment 7 – MapReduce][assignment-7]
- [Assignment 7 – MapReduce – Solution][solution-7]

**Required:**

- [<u>http://research.google.com/archive/mapreduce.html</u>][required-8-1]
- [<u>https://hadoop.apache.org/docs/r1.2.1/mapred_tutorial.html</u>][required-8-2]

**Recommended:**

- [<u>https://hadoopecosystemtable.github.io/</u>][recommended-8-1]
- [<u>https://www.tutorialspoint.com/lisp/lisp_overview.htm</u>][recommended-8-2]
- [<u>http://www.tutorialspoint.com/apache_spark/</u>][recommended-8-3]

------

### 12th December 2018 – 18th December 2018

- [Lecture 9 – Distributed File Systems][slide-9]
- [Assignment 8 – Web Caching][assignment-8]
- [Assignment 8 – Web Caching – Solution][solution-8]

**Required:**

- Ghemawat, Gobioff, and Leung. The Google file system. SOSP '03. ACM
- Operating Systems: Three Easy Pieces, Chapter 48: [http://pages.cs.wisc.edu/~remzi/OSTEP/dist-nfs.pdf][required-9]

**Recommended:**

- Shvachko, Hairong Kuang, Radia, Chansler, "The Hadoop Distributed File System," MSST 2010
- [<u>https://www.backblaze.com/blog/reed-solomon/</u>][recommended-9]

------

### 19th December 2018 – 25th December 2018

- [Lecture 10 – Peer-to-Peer Systems][slide-10]
- [Assignment 9 – Peer-to-Peer Systems – Part 1][assignment-9]
- [Assignment 9 – Peer-to-Peer Systems – Part 1 – Solution][solution-9]

**Required:**

- [[1][required-10-1]] Eng Keong Lua; Crowcroft, J.; Pias, M.; Sharma, R.; Lim, S.; "[<u>A survey and comparison of peer-to-peer overlay network schemes</u>][required-10-2]" Communications Surveys & Tutorials, IEEE , vol.7, no.2, pp. 72- 93, Second Quarter 2005. [Focus on Section I, II.A, II.B, II.D, IV.A, IV.B.]
- [[<u>2</u>][required-10-3]] Operating Systems: Three Easy Pieces, Chapter 48: http://pages.cs.wisc.edu/~remzi/OSTEP/dist-nfs.pdf
- [<u>Incentives build robustness in BitTorrent</u>][required-10-4]. Technical Report by Bram Cohen

**Recommended:**

- [[<u>1</u>][recommended-10-1]] Ion Stoica, Robert Morris, David Karger, M. Frans Kaashoek, and Hari Balakrishnan. 2001. Chord: A scalable peer-to-peer lookup service for internet applications. In Proceedings of the 2001 conference on Applications, technologies, architectures, and protocols for computer communications (SIGCOMM '01). ACM, New York, NY, USA, 149-160.
- [[<u>2</u>][recommended-10-2]] Sylvia Ratnasamy, Paul Francis, Mark Handley, Richard Karp, and Scott Shenker. 2001. A scalable content-addressable network. In Proceedings of the 2001 conference on Applications, technologies, architectures, and protocols for computer communications (SIGCOMM '01). ACM, New York, NY, USA, 161-172.
- [[<u>3</u>][recommended-10-3]] Antony I. T. Rowstron and Peter Druschel. 2001. Pastry: Scalable, Decentralized Object Location, and Routing for Large-Scale Peer-to-Peer Systems. In Proceedings of the IFIP/ACM International Conference on Distributed Systems Platforms Heidelberg (Middleware '01), Rachid Guerraoui (Ed.). Springer-Verlag, London, UK, UK, 329-350.

------

### 9th January 2019 – 15th January 2019

- [Lecture 11 – Publish/Subscribe Systems][slide-11]
- [Assignment 10 – Peer-to-Peer Systems – Part 2][assignment-10]
- [Assignment 10 – Peer-to-Peer Systems – Part 2 – Solution][solution-10]

**Required:**

- Patrick Th. Eugster, Pascal A. Felber, Rachid Guerraoui, and Anne-Marie Kermarrec. 2003. The many faces of publish/subscribe. ACM Comput. Surv. 35, 2 (June 2003), 114-131. DOI=[<u>http://dx.doi.org/10.1145/857076.857078</u>][required-11-1]

**Recommended:**

- Antonio Carzaniga, David S. Rosenblum, and Alexander L. Wolf. 2001. Design and evaluation of a wide-area event notification service. ACM Trans. Comput. Syst. 19, 3 (August 2001), 332-383. DOI=[<u>http://dx.doi.org/10.1145/380749.380767</u>][recommended-11-1]

------

### 16th January 2019 – 22nd January 2019

- [Lecture 12 – Blockchain][slide-12]
- [Assignment 11 – Pub/Sub Systems][assignment-11]
- [Assignment 11 – Pub/Sub Systems – Solution][solution-11]

**Required:**

- [<u>https://bitcoin.org/bitcoin.pdf</u>][required-12]

**Recommended:**

- [![Blockchain][recommended-12-2]][recommended-12-1]

------

### 23rd January 2019 – 29th January 2019

- [Lecture 13 – Virtualization, cloud and serverless computing][slide-13]
- [Assignment 12 – Blockchain][assignment-12]
- [Assignment 12 – Blockchain][solution-12]

**Required:**

- [<u>https://docs.docker.com/engine/docker-overview/</u>][required-13-1]
- [<u>https://openwhisk.apache.org/documentation.html</u>][required-13-2]

**Recommended:**

- [![Virtualization, cloud and serverless computing][recommended-13-2]][recommended-13-1]

------

### 30th January 2019 – 5th February 2019

- [Lecture 14 – Misc][slide-14]

**Recommended:**

- [<u>https://www.cs.cornell.edu/home/rvr/papers/OSDI04.pdf</u>][recommended-14-1]
- [![Miscellaneous][recommended-14-3]][recommended-14-2]

------

### 6th January 2019 – 12th February 2019

- [Lecture 15 – Review][slide-15]

[slide-1]:  Slides/01%20-%20Introduction,%20motivation%20and%20overview.pdf                 "Slide 1 — Introduction, motivation and overview"
[slide-2]:  Slides/02%20-%20Time.pdf                                                        "Slide 2 — Time"
[slide-3]:  Slides/03%20-%20Coordination%20and%20agreement.pdf                              "Slide 3 — Coordination and agreement"
[slide-4]:  Slides/04%20-%20Consensus%20with%20Paxos.pdf                                    "Slide 4 — Consensus with Paxos"
[slide-5]:  Slides/05%20-%20Replication.pdf                                                 "Slide 5 — Replication"
[slide-6]:  Slides/06%20-%20Consistency%20and%20transactions.pdf                            "Slide 6 — Consistency and transaction"
[slide-7]:  Slides/07%20-%20CAP%20theorem,%20web%20caching%20and%20consistent%20hashing.pdf "Slide 7 — CAP theorem, web caching and consistency hashing"
[slide-8]:  Slides/08%20-%20MapReduce.pdf                                                   "Slide 8 — MapReduce"
[slide-9]:  Slides/09%20-%20Distributed%20File%20Systems.pdf                                "Slide 9 — Distributed File Systems"
[slide-10]: Slides/10%20-%20Peer-to-Peer%20systems.pdf                                      "Slide 10 — Peer-to-Peer systems"
[slide-11]: Slides/11%20-%20Publish%20and%20subscribe%20systems.pdf                         "Slide 11 — Publish and subscribe systems"
[slide-12]: Slides/12%20-%20Blockchain.pdf                                                  "Slide 12 — Blockchain"
[slide-13]: Slides/13%20-%20Virtualization,%20cloud%20and%20serverless%20computing.pdf      "Slide 13 — Virtualization, cloud and serverless computing"
[slide-14]: Slides/14%20-%20Miscellaneous.pdf                                               "Slide 14 — Miscellaneous"
[slide-15]: Slides/15%20-%20Final%20exam%20review.pdf                                       "Slide 15 — Final exam review"

[assignment-1]:  Assignments/01%20-%20Physical%20clocks%20(Original).pdf                                           "Assignment 1 — Physical clocks"
[assignment-2]:  Assignments/02%20-%20Logical%20time%20(Original).pdf                                              "Assignment 2 — Logical time"
[assignment-3]:  Assignments/03%20-%20Coordination%20and%20agreement%20(Original).pdf                              "Assignment 3 — Coordination and agreement"
[assignment-4]:  Assignments/04%20-%20Paxos%20and%20Replicated%20State%20Machines%20(Original).pdf                 "Assignment 4 — Paxos and Replicated State Machines"
[assignment-5]:  Assignments/05%20-%20Replication%20(Original).pdf                                                 "Assignment 5 — Replication"
[assignment-6]:  Assignments/06%20-%20Consistency%20(Original).pdf                                                 "Assignment 6 — Consistency"
[assignment-7]:  Assignments/07%20-%20MapReduce%20(Original).pdf                                                   "Assignment 7 — MapReduce"
[assignment-8]:  Assignments/08%20-%20CAP%20theorem,%20web%20caching%20and%20consistent%20hashing%20(Original).pdf "Assignment 8 — CAP theorem, web caching and consistent hashing"
[assignment-9]:  Assignments/09%20-%20Peer-to-Peer%20systems%20(Original).pdf                                      "Assignment 9 — Peer-to-Peer systems (1)"
[assignment-10]: Assignments/10%20-%20Peer-to-Peer%20systems%20(Original).pdf                                      "Assignment 10 — Peer-to-Peer systems (2)"
[assignment-11]: Assignments/11%20-%20Publish%20and%20subscribe%20systems%20(Original).pdf                         "Assignment 11 — Publish and subscribe systems"
[assignment-12]: Assignments/12%20-%20Blockchain%20(Original).pdf                                                  "Assignment 12 — Blockchain"

[solution-1]:  Assignments/01%20-%20Physical%20clocks%20(Original%20solution).pdf                                           "Solution 1 — Physical clocks"
[solution-2]:  Assignments/02%20-%20Logical%20time%20(Original%20solution).pdf                                              "Solution 2 — Logical time"
[solution-3]:  Assignments/03%20-%20Coordination%20and%20agreement%20(Original%20solution).pdf                              "Solution 3 — Coordination and agreement"
[solution-4]:  Assignments/04%20-%20Paxos%20and%20Replicated%20State%20Machines%20(Original%20solution).pdf                 "Solution 4 — Paxos and Replicated State Machines"
[solution-5]:  Assignments/05%20-%20Replication%20(Original%20solution).pdf                                                 "Solution 5 — Replication"
[solution-6]:  Assignments/06%20-%20Consistency%20(Original%20solution).pdf                                                 "Solution 6 — Consistency"
[solution-7]:  Assignments/07%20-%20MapReduce%20(Original%20solution).pdf                                                   "Solution 7 — MapReduce"
[solution-8]:  Assignments/08%20-%20CAP%20theorem,%20web%20caching%20and%20consistent%20hashing%20(Original%20solution).pdf "Solution 8 — CAP theorem, web caching and consistent hashing"
[solution-9]:  Assignments/09%20-%20Peer-to-Peer%20systems%20(Original%20solution).pdf                                      "Solution 9 — Peer-to-Peer systems (1)"
[solution-10]: Assignments/10%20-%20Peer-to-Peer%20systems%20(Original%20solution).pdf                                      "Solution 10 — Peer-to-Peer systems (2)"
[solution-11]: Assignments/11%20-%20Publish%20and%20subscribe%20systems%20(Original%20solution).pdf                         "Solution 11 — Publish and subscribe systems"
[solution-12]: Assignments/12%20-%20Blockchain%20(Original%20solution).pdf                                                  "Solution 12 — Blockchain"

[required-1-1]:  https://www.rgoarchitects.com/Files/fallacies.pdf
[required-1-2]:  https://cs.brown.edu/courses/cs227/archives/2008/mitchpapers/required6.pdf
[required-1-3]:  http://it-ebooks.info/book/432/
[required-1-4]:  http://www.mpam.mp.br/attachments/article/6214/HBase%EF%BC%9AThe%20Definitive%20Guide.pdf
[required-4]:    http://research.microsoft.com/en-us/um/people/lamport/pubs/pubs.html#paxos-simple
[required-7]:    https://henryr.github.io/cap-faq/
[required-8-1]:  http://research.google.com/archive/mapreduce.html
[required-8-2]:  https://hadoop.apache.org/docs/r1.2.1/mapred_tutorial.html
[required-9]:    http://pages.cs.wisc.edu/~remzi/OSTEP/dist-nfs.pdf
[required-10-1]: https://www.cl.cam.ac.uk/research/dtg/www/files/publications/public/mp431/ieee-survey.pdf
[required-10-2]: http://dl.acm.org/citation.cfm?id=2211419
[required-10-3]: https://www.onion-router.net/Publications/IH-1996.pdf
[required-10-4]: http://www.bittorrent.org/bittorrentecon.pdf
[required-11-1]: http://dx.doi.org/10.1145/857076.857078
[required-12]:   https://bitcoin.org/bitcoin.pdf
[required-13-1]: https://docs.docker.com/engine/docker-overview/
[required-13-2]: https://openwhisk.apache.org/documentation.html

[recommended-1-1]:  http://java.sys-con.com/node/38665
[recommended-1-2]:  http://www.heise.de/open/artikel/NoSQL-im-Ueberblick-1012483.html?artikelseite=2
[recommended-1-3]:  http://wiki.apache.org/cassandra/
[recommended-1-4]:  http://de.slideshare.net/grro/cassandra-by-example-the-path-of-read-and-write-requests
[recommended-1-5]:  http://research.google.com/archive/spanner-osdi2012.pdf
[recommended-4]:    https://en.wikipedia.org/wiki/Paxos_(computer_science)
[recommended-5-1]:  https://youtu.be/FuP1Fvrv6ZQ?t=664                                                          "Apple Inc.: Cassandra Internals — Understanding Gossip"
[recommended-5-2]:  https://img.youtube.com/vi/FuP1Fvrv6ZQ/0.jpg
[recommended-6-1]:  https://youtu.be/em9zLzM8O7c?t=715                                                          "Consistency without consensus in production systems"
[recommended-6-2]:  https://img.youtube.com/vi/em9zLzM8O7c/0.jpg
[recommended-6-3]:  https://www.microsoft.com/en-us/research/wp-content/uploads/2016/02/tr-2003-96.pdf
[recommended-6-4]:  http://www.cs.cornell.edu/courses/cs734/2000fa/cached%20papers/SessionGuaranteesPDIS_1.html
[recommended-8-1]:  https://hadoopecosystemtable.github.io/
[recommended-8-2]:  https://www.tutorialspoint.com/lisp/lisp_overview.htm
[recommended-8-3]:  http://www.tutorialspoint.com/apache_spark/
[recommended-9]:    https://www.backblaze.com/blog/reed-solomon/
[recommended-10-1]: http://pdos.csail.mit.edu/papers/chord:sigcomm01/chord_sigcomm.pdf
[recommended-10-2]: http://conferences.sigcomm.org/sigcomm/2001/p13-ratnasamy.pdf
[recommended-10-3]: http://research.microsoft.com/en-us/um/people/antr/PAST/pastry.pdf
[recommended-11-1]: http://dx.doi.org/10.1145/380749.380767
[recommended-12-1]: https://youtu.be/l9jOJk30eQs
[recommended-12-2]: https://img.youtube.com/vi/l9jOJk30eQs/0.jpg
[recommended-13-1]: https://youtu.be/zLJbP6vBk2M
[recommended-13-2]: https://img.youtube.com/vi/zLJbP6vBk2M/0.jpg
[recommended-14-1]: https://www.cs.cornell.edu/home/rvr/papers/OSDI04.pdf
[recommended-14-2]: https://youtu.be/2BJyIIIYU8E
[recommended-14-3]: https://img.youtube.com/vi/2BJyIIIYU8E/0.jpg
